FROM registry.gitlab.com/qnib-golang/gonja-cli:v0-1-2 AS gonja

FROM bitnami/kubectl
ENTRYPOINT [ "/usr/bin/bash" ]
COPY --from=gonja /usr/local/bin/gonja-cli /usr/local/bin/

