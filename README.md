# gonja-kubectl

This is a simple tool to help you manage your kubernetes resources within CICD.

It packs `gonja-cli` and `kubectl` into a single container.

So you can have something like this.

```yaml
deploy:
  stage: deploy
  image: registry.gitlab.com/qnib-pub-containers/cicd/gonja-kubectl:v0.0.3
  script:
    - gonja-cli k8s/kube.cfg.tpl kube.cfg
    - gonja-cli k8s/patch-file.yaml.tpl patch-file.yaml
    - kubectl --kubeconfig kube.cfg -n ${K8S_NS} patch deployment golang-cli --patch-file patch-file.yaml
```
